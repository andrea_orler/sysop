/*
Anno Accademico: 2014/2015
Corso di Studio: Sistemi Operativi 1
Autori: Da Col Fabio	159569
		Orler Andrea	160430
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <getopt.h>
#include <ctype.h>
#include "file_manipulation.h"
#include "functions.h"
#include "giornata.h"
#include "partita.h"

FILE *config;
FILE *cleanconfig;
FILE *results;
FILE *ranking_file;

int main(int argc, char **argv)
{
	char* configurations = malloc(50);			// string containing the configuration file name						
	strcpy(configurations, "config.txt");
	int interactive = 0;						// flag for interactive matches
	int verbose = 0;											

	interactive = get_input(argc, argv, configurations, &verbose);	// gets the command-line options (implemented in "functions.h")

	/* Checks if failure happenes opening files */
	if ((config = fopen (configurations, "r")) == NULL)			
	{
		exit(printf("Failed to open \"%s\" file\n", configurations));
	}

	if ((cleanconfig = fopen ("cleanconfig.txt", "w")) == NULL)
	{
		exit(printf("Failed to create \"cleanconfig.txt\" file\n"));
	}
	/* ----- */

	clean_config(config, cleanconfig);					//creates a file 'cleanconfig' without comments and empty lines (implemented in "file_manipulation.h")

	/* Checks if failure happenes closing files */
	if (fclose(config) != 0)
	{
		exit(printf("Failed to close \"%s\" file\n", configurations));
	}
	if (fclose(cleanconfig) != 0)
	{
		exit(printf("Failed to close \"cleanconfig.txt\" file\n"));
	}
	/* ----- */

	/* Checks if failure happenes opening files */
	if ((cleanconfig = fopen ("cleanconfig.txt", "r")) == NULL)
	{
		exit(printf("Failed to open \"cleanconfig.txt\" file\n"));
	}
	/* ----- */

	int team_number;
	team_number = get_team_number(cleanconfig);			//gets the number of teams (implemented in "file_manipulation.h")
	int days = (team_number - 1) * 2;
	int match_number = team_number / 2;

	if (team_number % 2 != 0)
	{
		exit(printf("Cannot create a pool with an odd team number\n"));
	}

	char* teams [team_number];			//string array that will contain teams' names
	int ability [team_number];			//array that will contain teams' ability				

	/* allocation of shared memory for "shared_home" array */
	int segment_id_home;
	int *shared_home;
	const int size_home = sizeof(int [team_number/2]);
	segment_id_home = shmget (IPC_PRIVATE, size_home, S_IRUSR | S_IWUSR);
	if (segment_id_home == -1)
	{
		exit(printf("Faild to create \"shared_home\" shared memory\n"));
	}
	if ((shared_home = (int *) shmat (segment_id_home, NULL, 0)) == (void*) -1)
	{
		exit(printf("Process torneo could not attach to \"shared_home\" shared memory\n"));
	}
	/* ----- */

	/* allocation of shared memory for "shared_visitor" array */
	int segment_id_visitor;
	int *shared_visitor;
	const int size_visitor = sizeof(int [team_number/2]);
	segment_id_visitor = shmget (IPC_PRIVATE, size_visitor, S_IRUSR | S_IWUSR);
	if (segment_id_visitor == -1)
	{
		exit(printf("Faild to create \"shared_visitor\" shared memory\n"));
	}
	if ((shared_visitor = (int *) shmat (segment_id_visitor, NULL, 0)) == (void*) -1)
	{
		exit(printf("Process torneo could not attach to \"shared_visitor\" shared memory\n"));
	}
	/* ----- */

	/* allocation of shared memory for "shared_results" array */
	int segment_id_results;
	int *shared_results;
	const int size_results = sizeof(int [4 * (team_number - 1) * team_number]);
	segment_id_results = shmget (IPC_PRIVATE, size_results, S_IRUSR | S_IWUSR);
	if (segment_id_results == -1)
	{
		exit(printf("Faild to create \"shared_results\" shared memory\n"));
	}
	if ((shared_results = (int *) shmat (segment_id_results, NULL, 0)) == (void*) -1)
	{
		exit(printf("Process torneo could not attach to \"shared_results\" shared memory\n"));
	}
	/* ----- */


	get_teams(cleanconfig, teams, ability, team_number);	//gets name and ability of the teams 
															//and saves them in "teams" and "ability" arrays

	/* if the game is interactive, saves the user's team
	   uppercase to better locate it in results and ranking */
	if (interactive == 1)
	{
		int t = 0;
		while (teams[0][t])
		{
			teams[0][t] = toupper(teams[0][t]);
      		t++;
		}
	}

	/* Checks if failure happenes closing files */
	if (fclose(cleanconfig) != 0)
	{
		exit(printf("Failed to close \"cleanconfig.txt\" file\n"));
	}
	/* ----- */

	/* Splits the teams in "home" array and "visitor" array */
	int pool[team_number];						//array that will be used to randomize the calendar of matches
	pool_randomizer (pool, team_number);		//function that randomizes the array

 	int i;
    for (i = 0; i < team_number / 2; i++)		//splits the randomized teams in home and visitor arrays
    {
        shared_home [i] = pool[i];
        shared_visitor[i] = pool[team_number - 1 - i]; 
    }

	giornata(team_number, ability, segment_id_home, segment_id_visitor, segment_id_results, interactive, verbose, teams); //creates giornata processes

	struct rank ranking [team_number]; //struct containing the rankings; declared in "functions.h"

	int a; 
	for (a = 0; a < team_number; a++)
	{
		ranking[a].team = a;			//gives every team 0 points
		ranking[a].points = 0;	
	}

	/* prints and saves the results in order of day */
	if ((results = fopen("results.txt", "w")) == NULL)
	{
		exit(printf("Failed to create \"results.txt\" file\n"));
	}

	int j;
	for (j = 0; j < days; j++)
	{
		printf("Giornata %d:\n", j+1);
		if (fprintf(results, "Giornata %d:\n", j+1) < 0)
		{
			exit(printf("Failed to write in \"results.txt\" file\n"));
		}

		int t;
		for(t = 0; t < match_number; t++)
		{
			int index = j * (4 * match_number) + (t * 4);
			printf("\t%s - %s:\t%d - %d\n", teams[shared_results[index]], teams[shared_results[index + 2]], shared_results[index + 1], shared_results[index + 3]);
			if (fprintf(results, "\t%s - %s:\t%d - %d\n", teams[shared_results[index]], teams[shared_results[index + 2]], shared_results[index + 1], shared_results[index + 3]) < 0)
			{
				exit(printf("Failed to write in \"results.txt\" file\n"));
			}
			
			if (shared_results[index + 1] > shared_results[index + 3])			//if left team wins gets 3 points
			{
				ranking[shared_results[index]].points += 3;
			}
			else if (shared_results[index + 1] == shared_results[index + 3])	//if tie both teams get 1 point
			{
				ranking[shared_results[index]].points ++;
				ranking[shared_results[index + 2]].points ++;
			}
			else																//if right team wins gets 3 points
			{
				ranking[shared_results[index + 2]].points += 3;
			}
		}
		printf("\n");
	}
	if (fclose(results) != 0)
	{
		exit(printf("Failed to close \"results.txt\" file\n"));
	}
	/* ----- */

	quick_sort(ranking, 0, team_number - 1); //sorts the ranking

	/* prints the ranking */
	ranking_file = fopen("ranking.txt", "w");

	printf("Classifica:\n");
	if (fprintf(ranking_file, "Classifica:\n") < 0)
	{
		exit(printf("Failed to write in \"ranking.txt\" file\n"));
	}

	int b;
	for (b = team_number - 1; b >= 0; b--)
	{
		printf("\t%s\t%d\n", teams[ranking[b].team], ranking[b].points);
		if (fprintf(ranking_file, "\t%s\t%d\n", teams[ranking[b].team], ranking[b].points) < 0)
		{
			exit(printf("Failed to write in \"ranking.txt\" file\n"));
		}
	}
	
	if (fclose(ranking_file) != 0)
	{
		exit(printf("Failed to close \"ranking.txt\" file\n"));
	}
	/* ----- */

	/* releases shared memory */
	if ((shmctl (segment_id_results, IPC_RMID, (struct shmid_ds *) 0)) == -1)
	{
  		perror ("shmctl");
 		exit (-1);
	}
	if ((shmctl (segment_id_home, IPC_RMID, (struct shmid_ds *) 0)) == -1)
	{
  		perror ("shmctl");
 		exit (-1);
	}
	if ((shmctl (segment_id_visitor, IPC_RMID, (struct shmid_ds *) 0)) == -1)
	{
  		perror ("shmctl");
 		exit (-1);
	}
	/* ----- */
	return 0;
}