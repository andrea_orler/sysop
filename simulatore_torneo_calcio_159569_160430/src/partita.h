/*
Anno Accademico: 2014/2015
Corso di Studio: Sistemi Operativi 1
Autori: Da Col Fabio	159569
		Orler Andrea	160430
*/

#ifndef PARTITA_
#define PARTITA_
#include "functions.h"

void partita(int segment_id_team_ability, int interactive, int verbose, char *teams[])
{
	
	int pid = fork();

	if (pid < 0)
	{
		exit(printf("Processo partita non creato\n"));
	}

	else if (pid == 0) // process partita 
	{
		int score1 = 0;
		int score2 = 0;
		int mypid = getpid();			//gets the process id
		int ppid = getppid();			//gets the parent id
		int *shared_team_ability = (int *) shmat(segment_id_team_ability, NULL, 0); //attaches to the shared memory created by giornata process
		struct timeval curr_time;
		long r;
		int i;

		if (interactive == 1 || verbose == 1)  //if the game is interactive, prints the teams' names
		{
			printf("%s - %s\n", teams[shared_team_ability[0]], teams[shared_team_ability[2]]);
		}
		
		for (i = 0; i < 3; i++)			//for each of the 3 parts of the match
		{	
			gettimeofday(&curr_time);
			r = curr_time.tv_usec;
			int gol = 0;	//flag to know if the first team scores. If it does, the second team can score only after the first
			r = myrandom(r);
			int minutes1 = r % 30 + (i*30);	//randomly decided the minute of the possible gol between 0-30 or 30-60 or 60-90
			r = myrandom(r);
			if (r % 101 < shared_team_ability[1] * 5)
			{
				if (interactive == 1)		//if the match is interactive
				{
					if (shared_team_ability[0] == 0)		//if the attacking team is the user team
					{
						while (1)							//loops until the user digits 't' or 'p'
						{
							char action;
							printf("\t%d° minuto - azione d'attacco: (t)ira o (p)ara!\n", minutes1);
							scanf(" %c", &action);
							if (action == 't')
							{
								score1++;
								gol = 1;
								printf("\tGOL!!\n\t%s - %s: %d - %d\n", teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
								break;
							}
							if (action == 'p')
							{
								printf("\tPARATA!\n");
								break;
							}
							else
							{
								printf("\tDigitare 't' per tirare o 'p' per parare.\n");
							}
						}
					}

					else if (shared_team_ability[2] == 0)	//if the defending team is the user team
					{
						while (1)							//loops until the user digits 't' or 'p'
						{
							char action;
							printf("\t%d° minuto - tiro verso la tua porta: (t)ira o (p)ara!\n", minutes1);
							scanf(" %c", &action);
							if (action == 't')
							{
								score1++;
								gol = 1;
								printf("\tGol subito!!\n\t%s - %s: %d - %d\n", teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
								break;
							}
							if (action == 'p')
							{
								printf("\tPARATA!\n");
								break;
							}
							else
							{
								printf("\tDigitare 't' per tirare o 'p' per parare.\n");
							}
						}
					}

					else		//if this isn't the user team
					{
						score1++;
						gol = 1;
						if (verbose == 1)		//if verbose, prints the detail of every gol
						{
							printf("\t%d° minuto - gol %s\n\t%s - %s: %d - %d\n", minutes1, teams[shared_team_ability[0]], teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
							sleep(1);
						}
					}
				}

				else		// if the match is not interactive
				{
					score1++;
					gol = 1;
					if (verbose == 1)		//if verbose, prints the detail of every gol
					{
						printf("\t%d° minuto - gol %s\n\t%s - %s: %d - %d\n", minutes1, teams[shared_team_ability[0]], teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
						sleep(1);
					}
				}			
			}

			r = myrandom(r);
			int minutes2 = r % (30 - (gol * (minutes1 - i*30))) + (i * 30) + (gol * (minutes1 - i*30)); //decided the minute of the possible gol.
			r = myrandom(r);																			//If first team scores, cannot score before it.
			if (r % 101 < shared_team_ability[3] * 5)
			{
				if (interactive == 1)		//if the match is interactive
				{
					if (shared_team_ability[2] == 0)		//if the attacking team is the user team
					{
						while (1)							//loops until the user digits 't' or 'p'
						{
							char action;
							printf("\t%d° minuto - azione d'attacco: (t)ira o (p)ara!\n", minutes2);
							scanf(" %c", &action);
							if (action == 't')
							{
								score2++;
								printf("\tGOL!!\n\t%s - %s: %d - %d\n", teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
								break;
							}
							if (action == 'p')
							{
								printf("\tPARATA!\n");
								break;
							}
							else
							{
								printf("\tDigitare 't' per tirare o 'p' per parare.\n");
							}
						}
					}

					else if (shared_team_ability[0] == 0)	//if the defending team is the user team
					{
						while (1)							//loops until the user digits 't' or 'p'
						{
							char action;
							printf("\t%d° minuto - tiro verso la tua porta: (t)ira o (p)ara!\n", minutes2);
							scanf(" %c", &action);
							if (action == 't')
							{
								score2++;
								printf("\tGol subito!!\n\t%s - %s: %d - %d\n", teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
								break;
							}
							if (action == 'p')
							{
								printf("\tPARATA!\n");
								break;
							}
							else
							{
								printf("\tDigitare 't' per tirare o 'p' per parare.\n");
							}
						}
					}

					else		//if this isn't the user team
					{
						score2++;
						if (verbose == 1)		//if verbose, prints the detail of every gol
						{
							printf("\t%d° minuto - gol %s\n\t%s - %s: %d - %d\n", minutes2, teams[shared_team_ability[2]], teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
							sleep(1);
						}
					}
				}
				else		//if the match isn't interactive
				{
					score2++;
					if (verbose == 1)		//if verbose, prints the detail of every gol
					{
						printf("\t%d° minuto - gol %s\n\t%s - %s: %d - %d\n", minutes2, teams[shared_team_ability[2]], teams[shared_team_ability[0]], teams[shared_team_ability[2]], score1, score2);
						sleep(1);
					}
				}
			}
			printf("\t%d° minuto \n", 30*(i+1));
		}
		
		shared_team_ability[1] = score1;	//loads the results exploiting the shred_team_ability array in shared memory
		shared_team_ability[3] = score2;
		exit(pid);
	}
	else //process giornata
	{
		wait(pid);
	}
}
#endif //PARTITA_