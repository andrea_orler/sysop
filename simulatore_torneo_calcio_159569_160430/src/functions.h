/*
Anno Accademico: 2014/2015
Corso di Studio: Sistemi Operativi 1
Autori: Da Col Fabio    159569
        Orler Andrea    160430
*/

#ifndef FUNCTIONS_
#define FUNCTIONS_

struct rank
    {
        int team;
        int points;
    };

int get_input (int argc, char **argv, char* configurations, int *verbose)
{
    char c;
    int interactive = 0;
    static struct option long_options[] = 
    {
        {"config", required_argument, NULL,  'c'},
        {"interactive", no_argument, NULL, 'i'},
        {"verbose", no_argument, NULL, 'v'},
        {0, 0, 0, 0}
    };
    while ((c = getopt_long(argc, argv, "c:iv", long_options, NULL)) != -1)
    {
        switch (c) 
        {
            case 'c':
                strcpy(configurations, optarg);
                break;

            case 'i':
                interactive = 1;
                break;

            case 'v':
                *verbose = 1;
                break;

            default:
                exit(printf("Wrong usage. Possible options: \n\t-c/--config <config-file.txt>\n\t-i/--interactive\n\t-v/--verbose\n"));
        }
    }
    return interactive;
}


void pool_randomizer (int pool[], int team_number)      //Creates a randomly sorted array 
{                                                       //with integers in range [0 - team_number]
    int i, size;
    int aux[team_number];                               //auxiliar array to randomize pool
    
    time_t t;
    srand((unsigned)time(&t));

    for (i = 0; i < team_number; ++i)                   //Initializes the auxiliar array
    {
        aux[i] = i;
 	}
    
    for (i = 0, size = team_number; size > 1; --size)   //Randomly sorts the array
    {
        int pos;
        pos = rand() % size;                            
        i++;
        pool[i-1] = aux[pos];                           
        aux[pos] = aux[size - 1];
    }
    pool[i] = aux[0];
}

long myrandom(long r)                                       //randomizes using module. The numbers have been chosen to prevent repetitions
{
    return (r = ((r * 7621) + 1) % 32768);
}

void quick_sort (struct rank ranking [], int a, int z)      //quick_sort implementation
{
    int j;
    if (a < z) 
    {
        j = partition(ranking, a, z);
        quick_sort(ranking, a, j - 1);
        quick_sort(ranking, j + 1, z);
    }
}

int partition(struct rank ranking [], int a, int z)         // quick_sort auxiliar function
{
    struct rank pivot, t;
    int i, j;
    pivot = ranking[a];
    i = a; 
    j = z + 1;
        
    while(1)
    {
        do i++; 
        while (ranking[i].points <= pivot.points && i <= z);

        do j--;
        while (ranking[j].points > pivot.points);

        if( i >= j ) break;

        t = ranking[i];
        ranking[i] = ranking[j];
        ranking[j] = t;
    }

    t = ranking[a];
    ranking[a] = ranking[j];
    ranking[j] = t;

    return j;
}

#endif //FUNCTIONS_