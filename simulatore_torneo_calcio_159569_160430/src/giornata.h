/*
Anno Accademico: 2014/2015
Corso di Studio: Sistemi Operativi 1
Autori: Da Col Fabio	159569
		Orler Andrea	160430
*/

#ifndef GIORNATA_
#define GIORNATA_
#include "partita.h"


void giornata(int team_number, int *ability, int segment_id_home, int segment_id_visitor, int segment_id_results, int interactive, int verbose, char* teams[])
{
	pid_t pid;
	int days = (team_number - 1) * 2;
	int match_number = team_number / 2;
	/*create "giornata" processes */
	int t;	
	for(t = 0; t < days; t++)		//create a number of processes equal to the days	
	{
		pid = fork(); 

		if (pid < 0) 
		{ 
			exit(printf("errore\n")); 
		} 
	
		else if (pid == 0) 		//process giornata
		{
			/* attaches to shared memory segments created by torneo process */
			int *shared_home;
			if ((shared_home = (int *) shmat (segment_id_home, NULL, 0)) == (void*) -1)			//contains "home" teams
			{
				exit(printf("Process torneo could not attach to \"shared_home\" shared memory\n"));
			}

			int *shared_visitor;
			if ((shared_visitor = (int *) shmat (segment_id_visitor, NULL, 0)) == (void*) -1)		//contains "visitor" teams
			{
				exit(printf("Process torneo could not attach to \"shared_visitor\" shared memory\n"));
			}

			int *shared_results;
			if ((shared_results = (int *) shmat (segment_id_results, NULL, 0)) == (void*) -1)		//for every match played, contains the team and the relative amount of goals scored
			{
				exit(printf("Process torneo could not attach to \"shared_results\" shared memory\n"));
			}	
			/* ----- */
			
			/* creation of a shared memory for "team_ability" */
			int segment_id_team_ability;
			int *shared_team_ability;
			const int size_team_ability = sizeof(int [4]);	
			segment_id_team_ability = shmget (IPC_PRIVATE, size_team_ability, S_IRUSR | S_IWUSR);
			if (segment_id_team_ability == -1)
			{
				exit (printf("Error during creation of shared memory!\n"));
			}
			if ((shared_team_ability = (int *) shmat (segment_id_team_ability, NULL, 0)) == (void*) -1)
			{
				exit(printf("Process torneo could not attach to \"shared_team_ability\" shared memory\n"));
			}
			/* ----- */

			int y;
			int match_number = team_number / 2;

			for (y = 0; y < match_number; y++)											//initializes "shared_team_ability" array as follows:
			{	/* match setup */														//	+ - - - - + - - - - + - - - - + - - - - +
				if (t < days / 2)														//	|         |         |         |         |
				{																		//	|  team1  |ability1 |  team2  |ability2 |
					shared_team_ability[0] = shared_home [y];							//	|         |         |         |         |
					shared_team_ability[1] = ability [shared_home[y]];					//	+ - - - - + - - - - + - - - - + - - - - +
					shared_team_ability[2] = shared_visitor [y];						// where team1 and team2 are the opponing teams
					shared_team_ability[3] = ability [shared_visitor[y]];				// in the match started by partita()
				}

				else
				{																		//in the second round (ritorno) match visitors are home 
					shared_team_ability[0] = shared_visitor [y];						//and home are visitors
					shared_team_ability[1] = ability [shared_visitor[y]];
					shared_team_ability[2] = shared_home [y];
					shared_team_ability[3] = ability [shared_home[y]];
				}
				/* ----- */
				partita(segment_id_team_ability, interactive, verbose, teams);					//the match begins

				int i;
				for (i = 0; i < 4; ++i)
				{
				 	shared_results[(t * (4 * match_number)) + (y * 4) + i] = shared_team_ability[i]; //puts the content of "shared_team_ability" in the right place in "shared_ results"
				}

			}															

	        /*  Shifts the arrays home and visitor as follows.
        		At every shift the columns will provide the matches to play.
    
		                + - - + - - + - - + - - +           + - - + - - + - - + - - +
		    home:       |  0  |  1  |  2  |  3  |   shift   |  0  |  2  |  3  |  7  |
		                + - - + - - + - - + - - +    -->    + - - + - - + - - + - - +
		    visitor:    |  4  |  5  |  6  |  7  |           |  1  |  4  |  5  |  6  |
		                + - - + - - + - - + - - +           + - - + - - + - - + - - +
		    */      
	        int pivot = shared_home[0];											//fixes the pivot element
	        int c;
	        int right = shared_visitor[(team_number / 2) - 1];
	        for (c = (team_number / 2) - 1; c > 0 ; c--)    					//visitor is shifted right (overflowing element saved in "right",
	        {                                               					//first element replaced with the first non-pivot element of "home")
	        	shared_visitor[c] = shared_visitor[c-1];
	        }
	        shared_visitor[0] = shared_home[1];
	 
	        int a;
	        for (a = 1; a <= (team_number / 2) - 1 + (team_number % 2); a++)    //home shifted left, last element replaced with "right"
	        {
	        	shared_home[a-1] = shared_home[a];
	        }
	        shared_home[(team_number / 2) - 1 + (team_number % 2)] = right;

	        shared_home[0] = pivot;                                             //restoring pivot
	        /*      ----------      */
			exit(pid);
		} 

		else //process torneo
		{	
			wait(pid);  
		}	
	}
}

#endif //GIORNATA_