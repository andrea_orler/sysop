/*
Anno Accademico: 2014/2015
Corso di Studio: Sistemi Operativi 1
Autori: Da Col Fabio	159569
		Orler Andrea	160430
*/

#ifndef FILE_MANIPULATION_
#define FILE_MANIPULATION_

void clean_config (FILE *config, FILE *cleanconfig)
{
	char buffer[256];

	if (config == NULL)
	{
    	exit(printf("Non riesco a caricare le configurazioni.\n"));
	}
	
	while(fgets(buffer, 255, config) != NULL)
	{
	 	if (buffer[0]!='#' && buffer[0]!='\n' && buffer[0]!='\r')			//drops comments and empty lines
	 	{
	 		if (fprintf(cleanconfig, "%s", buffer) < 0)   					//write on a second file
	 		{
	 			exit(printf("Failed to write in \"cleanconfig.txt\" file\n"));
	 		}
	 	}
   	}
}

int get_team_number(FILE* cleanconfig)
{
	char buffer[256];
	int team_number;
	if ((fgets(buffer, 256, cleanconfig)) == NULL)				//reads the first line in buffer
	{
		exit(printf("Could not retrieve the team number from configuration file\n"));
	}						
	team_number = atoi(buffer);					 
	return team_number;											//returns the number of teams
}

void get_teams(FILE* cleanconfig, char* teams[], int ability[], int team_number)
{
	char buffer[256];
	char team_name[50];
	char temp_ability[2];

	int i;
	for (i = 0; i < team_number; i++)
	{
		fgets(buffer, 256, cleanconfig);

		teams[i] = malloc(50);
		int k = 0;
		while (buffer[k] != ' ')								//saves anything before the first blank in team_name
		{
			team_name[k] = buffer[k];							
			k++;
		}
		team_name[k] = '\0';						
		strcpy(teams[i], team_name);							//moves team_name in the teams array
		temp_ability[0] = buffer[k+1];
		temp_ability[1] = buffer[k+2];
		ability[i] = atoi(temp_ability);						//saves the ability in the relative array
	}
}

#endif // FILE_MANIPULATION_